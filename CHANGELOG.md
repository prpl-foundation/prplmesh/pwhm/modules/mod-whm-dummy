# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.0 - 2023-01-17(14:30:09 +0000)

### Other

- load datamodel extension (definition and defaults)

## Release v0.1.2 - 2022-11-25(12:59:13 +0000)

### Other

- squash commits before opensourcing them to make sahbot principal author for SoftAtHome deliveries
- [pwhm] crash on stop

## Release v0.1.1 - 2022-09-29(15:08:26 +0000)

### Other

- opensource mod-whm-dummy

## Release v0.1.0 - 2022-09-29(07:51:10 +0000)

### Other

- dummy pwhm vendor module

