include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: all
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(DEST)/etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT).so $(DEST)/usr/lib/amx/wld/modules/$(COMPONENT).so

package: all
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(PKGDIR)/etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/wld/modules/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/wld/modules/$(COMPONENT).so
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT).odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

.PHONY: all clean changelog install package doc